# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/19 19:26:33 by bluff             #+#    #+#              #
#    Updated: 2020/02/28 18:56:35 by fle-roy          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

LIBFT_DIR = libft
PRINTF_DIR = ft_printf
GNL_DIR = gnl
LIBFT_DEP_DIR = libft_dep
PRINTF_DEP_DIR = printf_dep
GNL_DEP_DIR = gnl_dep
BTREE_DIR=btree
BTREE_TEST_DIR=__tests_btree
LIBFT_TEST_DIR=__tests_libft
BTREE_DEP_DIR=btree_dep
SRC_BTREE = $(addprefix $(BTREE_DIR)/, \
btree.c \
btree_movements.c \
btree_utils.c \
rbtree.c)

SRC_BTREE_TEST = $(addprefix $(BTREE_TEST_DIR)/, \
btree.test.c)


SRC_GNL = $(addprefix $(GNL_DIR)/, get_next_line.c)

SRC_LIBFT = $(addprefix $(LIBFT_DIR)/, ft_any.c ft_atoi.c ft_bzero.c \
ft_count_if.c ft_factorial.c ft_find_next_prime.c ft_foreach.c ft_isalnum.c \
ft_isalpha.c ft_isascii.c ft_isdigit.c ft_islowercase.c ft_isprime.c \
ft_isprint.c ft_issort.c ft_isuppercase.c ft_itoa.c ft_lstat.c \
ft_lstfind.c ft_lstforeach.c ft_lstforeach_if.c ft_lstlast.c \
ft_lstmerge.c ft_lstpush_back.c ft_lstpush_front.c ft_lstremove_if.c \
ft_lstreverse.c ft_lstsize.c ft_lstsort.c ft_lstadd.c ft_lstdel.c \
ft_lstdelone.c ft_lstiter.c ft_lstmap.c ft_lstnew.c ft_map.c ft_memalloc.c \
ft_memccpy.c ft_memchr.c ft_memcmp.c ft_memcpy.c ft_memdel.c ft_memmove.c \
ft_memset.c ft_pow.c ft_putchar.c ft_putchar_fd.c \
ft_putendl.c ft_putendl_fd.c ft_putnbr.c ft_putnbr_fd.c ft_putstr.c \
ft_putstr_fd.c ft_range.c ft_sqrt.c ft_strcat.c ft_strchr.c ft_strclr.c \
ft_strcmp.c ft_strcpy.c ft_strdel.c ft_strdup.c ft_strequ.c ft_striter.c \
ft_striteri.c ft_strjoin.c ft_strlcat.c ft_strlen.c ft_strmap.c ft_strmapi.c \
ft_strncat.c ft_strncmp.c ft_strncpy.c ft_strnequ.c ft_strnew.c ft_strnstr.c \
ft_strrchr.c ft_strrev.c ft_strcommon.c ft_strsplit.c ft_strstr.c ft_strsub.c \
ft_swap.c ft_tolower.c ft_toupper.c ft_atoi_base.c ft_iswhitespace.c \
ft_perror.c ft_itoa_base.c ft_numlen.c ft_getenv.c ft_free2d.c \
ft_str2ddup.c ft_haschar.c ft_strsplit_multi.c ft_lstdestroy.c \
dbuf_init.c dbuf_append.c dbuf_clear.c dbuf_insert.c dbuf_remove.c \
dbuf_destroy.c dbuf_substract.c dbuf_print.c \
ft_strndup.c ft_stralike.c ft_stricmp.c ft_atoi_hex.c ft_memdup.c)

SRC_LIBFT_TEST = $(addprefix $(LIBFT_TEST_DIR)/, \
pdbuf.test.c)

SRC_PRINTF = $(addprefix $(PRINTF_DIR)/, ft_printf_dbuf.c ft_printf_routine.c \
		ft_printf.c ft_printf_utils.c ft_printf_string.c ft_printf_parser.c \
		ft_printf_parameters.c ft_printf_utils2.c ft_printf_numbers.c \
		ft_printf_utils3.c ft_printf_numbers_upper.c ft_printf_wstring.c \
		ft_printf_utils4.c ft_printf_extract_dynamic_param.c \
		ft_printf_numbers2.c ft_printf_dbuf2.c)

INCLUDE = include
CFLAG =-g3 -Wall -Wextra -Werror -I $(INCLUDE)
OBJ = obj
CC = clang
LN = ar
LFLAGS = rsc
BIN = bin
OBJ_GNL = $(SRC_GNL:$(GNL_DIR)/%.c=$(OBJ)/%.o)
OBJ_BTREE = $(SRC_BTREE:$(BTREE_DIR)/%.c=$(OBJ)/%.o)
OBJ_BTREE_TEST = $(SRC_BTREE_TEST:$(BTREE_TEST_DIR)/%.c=$(OBJ)/%.o)
OBJ_LIBFT_TEST = $(SRC_LIBFT_TEST:$(LIBFT_TEST_DIR)/%.c=$(OBJ)/%.o)
OBJ_PRINTF = $(SRC_PRINTF:$(PRINTF_DIR)/%.c=$(OBJ)/%.o)
OBJ_LIBFT = $(SRC_LIBFT:$(LIBFT_DIR)/%.c=$(OBJ)/%.o)
DEP_LIBFT = $(SRC_LIBFT:$(LIBFT_DIR)/%.c=$(LIBFT_DEP_DIR)/%.d)
DEP_PRINTF = $(SRC_PRINTF:$(PRINTF_DIR)/%.c=$(PRINTF_DEP_DIR)/%.d)
DEP_GNL = $(SRC_GNL:$(GNL_DIR)/%.c=$(GNL_DEP_DIR)/%.d)
DEP_BTREE = $(SRC_BTREE:$(BTREE_DIR)/%.c=$(BTREE_DEP_DIR)/%.d)
BTREE_TEST_BIN = btree_test_bin
LIBFT_TEST_BIN = libft_test_bin
ifeq ($(CODE_COVERAGE),true)
	CFLAG += -fprofile-arcs -ftest-coverage
endif

NAME = $(addprefix ./$(BIN)/, libft.a)
all: $(NAME)
test: $(LIBFT_TEST_BIN) $(BTREE_TEST_BIN)
$(BTREE_TEST_BIN): $(NAME) $(OBJ_BTREE_TEST)
	@$(CC) $(CFLAG) -lcriterion $(OBJ_BTREE_TEST) $(NAME) -o $(BTREE_TEST_BIN) 
	@printf "\r\033[K[BTREE_TEST] \033[1;32mDone!\033[0m\n"
$(LIBFT_TEST_BIN): $(NAME) $(OBJ_LIBFT_TEST)
	@$(CC) $(CFLAG) -lcriterion $(OBJ_LIBFT_TEST) $(NAME) -o $(LIBFT_TEST_BIN) 
	@printf "\r\033[K[LIBFT_TEST] \033[1;32mDone!\033[0m\n"
$(LIBFT_DEP_DIR)/%.d: $(LIBFT_DIR)/%.c
	@printf "\r\033[K[LIBFT] \033[1;32mGenerating deps - $<\033[0m"
	@$(CC) $(CFLAG) -c -MM $^ | sed -e '1s/^/$(OBJ)\//' > $@
$(PRINTF_DEP_DIR)/%.d: $(PRINTF_DIR)/%.c
	@printf "\r\033[K[PRINTF] \033[1;32mGenerating deps - $<\033[0m"
	@$(CC) $(CFLAG) -c -MM $^ | sed -e '1s/^/$(OBJ)\//' > $@
$(GNL_DEP_DIR)/%.d: $(GNL_DIR)/%.c
	@printf "\r\033[K[GNL] \033[1;32mGenerating deps - $<\033[0m"
	@$(CC) $(CFLAG) -c -MM $^ | sed -e '1s/^/$(OBJ)\//' > $@
$(BTREE_DEP_DIR)/%.d: $(BTREE_DIR)/%.c
	@printf "\r\033[K[BTREE] \033[1;32mGenerating deps - $<\033[0m"
	@$(CC) $(CFLAG) -c -MM $^ | sed -e '1s/^/$(OBJ)\//' > $@
$(OBJ)/%.o: $(LIBFT_DIR)/%.c
	@printf "\r\033[K[LIBFT] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $< -o $@
$(OBJ)/%.o: $(PRINTF_DIR)/%.c
	@printf "\r\033[K[PRINTF] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $< -o $@
$(OBJ)/%.o: $(GNL_DIR)/%.c
	@printf "\r\033[K[GNL] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $< -o $@
$(OBJ)/%.o: $(BTREE_DIR)/%.c
	@printf "\r\033[K[BTREE] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $< -o $@
$(OBJ)/%.o: $(BTREE_TEST_DIR)/%.c
	@printf "\r\033[K[BTREE] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $< -o $@
$(OBJ)/%.o: $(LIBFT_TEST_DIR)/%.c
	@printf "\r\033[K[LIBFT] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $< -o $@
$(NAME): $(OBJ_LIBFT) $(OBJ_PRINTF) $(OBJ_GNL) $(OBJ_BTREE)
	@printf "\r\033[K[LIBFT_TOTAL] \033[1;32mLinking...\033[0m"
	@$(LN) $(LFLAGS) $(NAME) $(OBJ_LIBFT) $(OBJ_PRINTF) $(OBJ_GNL) $(OBJ_BTREE)
	@printf "\r\033[K[LIBFT_TOTAL] \033[1;32mDone!\033[0m\n"
clean:
	@rm -f $(OBJ_LIBFT) $(OBJ_PRINTF) $(OBJ_GNL)
	@printf "\r\033[K[LIBFT_TOTAL] \033[1;31mCleaned .o!\033[0m\n"
dclean:
	@rm -f $(DEP_LIBFT)
	@rm -f $(DEP_PRINTF)
	@rm -f $(DEP_GNL)
	@rm -f $(DEP_BTREE)
	@printf "\r\033[K[LIBFT_TOTAL] \033[1;31mCleaned .d!\033[0m\n"
fclean: clean
	@rm -f $(NAME)
	@printf "\r\033[K[LIBFT_TOTAL] \033[1;31mCleaned .a!\033[0m\n"
re:
	@$(MAKE) dclean
	@$(MAKE) fclean
	@$(MAKE) all
-include $(DEP_LIBFT)
-include $(DEP_PRINTF)
-include $(DEP_GNL)
-include $(DEP_BTREE)

.PHONY: all clean fclean re libft gnl printf
