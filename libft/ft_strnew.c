/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bluff <bluff@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 20:22:11 by bluff             #+#    #+#             */
/*   Updated: 2019/03/13 14:39:15 by bluff            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strnew(size_t size)
{
	void *res;

	if (!(res = malloc(size + 1)))
		ft_perror_fixed(MALLOC_ERROR_MSG, MALLOC_ERROR_MSG_LEN);
	ft_memset(res, '\0', size + 1);
	return (res);
}
