/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dbuf_clear.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 11:07:01 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 10:30:47 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		dbuf_clear(t_dbuf *buf)
{
	if (!buf || !buf->buf)
		return (LIBFT_ERR);
	ft_bzero(buf->buf, buf->len);
	buf->cursor = 0;
	return (LIBFT_OK);
}

int		pdbuf_clear(t_pdbuf *buf)
{
	if (!buf)
		return (LIBFT_ERR);
	ft_bzero(buf->buf, buf->cursor);
	buf->cursor = 0;
	return (LIBFT_OK);
}
