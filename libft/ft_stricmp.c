/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stricmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/21 00:12:49 by bluff             #+#    #+#             */
/*   Updated: 2019/05/13 11:19:51 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_stricmp(const char *lhs, const char *rhs)
{
	while (*lhs && *rhs && ft_tolower(*lhs) == ft_tolower(*rhs))
	{
		lhs++;
		rhs++;
	}
	return ((unsigned char)ft_tolower(*lhs) - (unsigned char)ft_tolower(*rhs));
}
