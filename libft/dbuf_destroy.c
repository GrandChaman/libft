/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dbuf_destroy.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 11:03:15 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 10:31:16 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <unistd.h>

int		dbuf_destroy(t_dbuf *buf)
{
	if (!buf)
		return (LIBFT_ERR);
	free(buf->buf);
	buf->buf = NULL;
	buf->len = 0;
	buf->cursor = 0;
	return (LIBFT_OK);
}

int		pdbuf_destroy(t_pdbuf *buf)
{
	if (!buf)
		return (LIBFT_ERR);
	buf->len = 0;
	buf->cursor = 0;
	if (buf->fd >= 0)
		close(buf->fd);
	buf->fd = -1;
	return (LIBFT_OK);
}
