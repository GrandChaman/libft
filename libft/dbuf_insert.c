/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dbuf_insert.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 11:42:04 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/20 14:51:09 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int		dbuf_insert(t_dbuf *buf, unsigned long pos, char to_insert)
{
	char *tmp;

	if (!buf || pos > buf->cursor)
		return (LIBFT_ERR);
	if ((uint64_t)(buf->cursor + 1) > buf->len)
	{
		tmp = ft_strnew(buf->len + BUFF_SIZE);
		ft_memcpy(tmp, buf->buf, pos + 1);
		tmp[pos] = to_insert;
		ft_memcpy(tmp + pos + 1, buf->buf + pos, buf->cursor - pos);
		free(buf->buf);
		buf->len += BUFF_SIZE;
		buf->buf = tmp;
	}
	else
	{
		ft_memmove(buf->buf + pos + 1, buf->buf + pos, buf->cursor - pos);
		buf->buf[pos] = to_insert;
	}
	buf->cursor++;
	return (LIBFT_OK);
}

int		pdbuf_push_char(t_pdbuf *buf, char to_insert)
{
	if (!buf)
		return (LIBFT_ERR);
	return (pdbuf_append_w_len(buf, &to_insert, 1));
}
