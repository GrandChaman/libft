/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bluff <bluff@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/31 12:23:57 by fle-roy           #+#    #+#             */
/*   Updated: 2019/03/13 14:39:18 by bluff            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strrev(char *str)
{
	int		i;
	int		s;
	char	*tmp;

	s = ft_strlen(str);
	if (!(tmp = (char*)malloc(sizeof(char) * (s + 1))))
		ft_perror_fixed(MALLOC_ERROR_MSG, MALLOC_ERROR_MSG_LEN);
	i = -1;
	while (++i < s)
		tmp[(s - 1) - i] = str[i];
	ft_strcpy(str, tmp);
	free(tmp);
	return (str);
}
