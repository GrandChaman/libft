/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/20 13:26:20 by bluff             #+#    #+#             */
/*   Updated: 2019/06/12 10:58:02 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t count)
{
	void	*bckup;

	bckup = dest;
	while (count--)
		*((unsigned char*)dest++) = *((unsigned char*)src++);
	return (bckup);
}

void	*ft_memrcpy(void *dest, const void *src, size_t count)
{
	void	*bckup;

	bckup = dest;
	while (count--)
		*((unsigned char*)dest++) = *((unsigned char*)src + count);
	return (bckup);
}
