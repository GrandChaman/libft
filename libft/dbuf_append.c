/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dbuf_append.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 11:00:15 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/20 14:50:36 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <unistd.h>

int		dbuf_append(t_dbuf *buf, char *to_append)
{
	unsigned long	len;
	unsigned long	i;
	char			*tmp;

	if (!buf || !buf->buf || !to_append || !buf->len)
		return (LIBFT_ERR);
	i = buf->len / BUFF_SIZE;
	len = ft_strlen(to_append);
	if (buf->cursor + len > buf->len)
	{
		while (buf->cursor + len > i)
			i += BUFF_SIZE;
		tmp = ft_strnew(i);
		buf->len = i;
		ft_memcpy(tmp, buf->buf, buf->cursor);
		free(buf->buf);
		buf->buf = tmp;
	}
	ft_strcpy(buf->buf + buf->cursor, to_append);
	buf->cursor += len;
	return (LIBFT_OK);
}

int		dbuf_append_w_len(t_dbuf *buf, char *to_append, size_t len)
{
	unsigned long	i;
	char			*tmp;

	if (!buf || !buf->buf || !to_append || !buf->len)
		return (LIBFT_ERR);
	i = buf->len / BUFF_SIZE;
	if (buf->cursor + len > buf->len)
	{
		while (buf->cursor + len > i)
			i += BUFF_SIZE;
		tmp = ft_strnew(i);
		buf->len = i;
		ft_memcpy(tmp, buf->buf, buf->cursor);
		free(buf->buf);
		buf->buf = tmp;
	}
	ft_memcpy(buf->buf + buf->cursor, to_append, len);
	buf->cursor += len;
	return (LIBFT_OK);
}

int		pdbuf_append_w_len(t_pdbuf *buf, char *to_append, size_t len)
{
	unsigned long	i;

	if (!buf || !to_append)
		return (LIBFT_ERR);
	if (buf->cursor == BUFF_SIZE && (pdbuf_print(buf) < 0))
		return (LIBFT_ERR);
	i = 0;
	while ((uint64_t)(len - i) > (uint64_t)(BUFF_SIZE - buf->cursor))
	{
		ft_strncpy(buf->buf + buf->cursor, to_append + i,
			BUFF_SIZE - buf->cursor);
		i += BUFF_SIZE - buf->cursor;
		buf->cursor = BUFF_SIZE - buf->cursor;
		if (pdbuf_print(buf) < 0)
			return (LIBFT_ERR);
	}
	ft_memcpy(buf->buf + buf->cursor, to_append + i, len - i);
	buf->cursor += len - i;
	return (LIBFT_OK);
}

int		pdbuf_append(t_pdbuf *buf, char *to_append)
{
	if (!buf || !to_append)
		return (LIBFT_ERR);
	return (pdbuf_append_w_len(buf, to_append, ft_strlen(to_append)));
}
