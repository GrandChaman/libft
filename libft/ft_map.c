/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bluff <bluff@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 10:20:56 by fle-roy           #+#    #+#             */
/*   Updated: 2019/03/13 14:38:43 by bluff            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>
#include <stdlib.h>

int	*ft_map(int *tab, int length, int (*f)(int))
{
	int *ntab;
	int i;

	i = 0;
	if (!(ntab = (int*)malloc(sizeof(int) * (length + 1))))
		ft_perror_fixed(MALLOC_ERROR_MSG, MALLOC_ERROR_MSG_LEN);
	while (i < length)
	{
		ntab[i] = f(tab[i]);
		i++;
	}
	return (ntab);
}
