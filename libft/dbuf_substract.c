/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dbuf_substract.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 16:36:42 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 09:22:39 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		dbuf_substract(t_dbuf *buf)
{
	if (!buf || buf == 0)
		return (LIBFT_ERR);
	buf->buf[--buf->cursor] = 0;
	return (LIBFT_OK);
}

void	dbuf_pop_front(t_dbuf *buf, size_t nb)
{
	if (!buf || buf == 0 || nb > buf->cursor)
		return ;
	ft_bzero(buf->buf + buf->cursor - nb, buf->cursor - nb);
	buf->cursor -= nb;
}

void	dbuf_pop_back(t_dbuf *buf, size_t nb)
{
	if (!buf || buf == 0 || nb > buf->cursor)
		return ;
	ft_memmove(buf->buf, buf->buf + nb, buf->cursor - nb);
	ft_bzero(buf->buf + buf->cursor - nb, nb);
	buf->cursor -= nb;
}
