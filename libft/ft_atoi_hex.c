/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_hex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 15:48:30 by bluff             #+#    #+#             */
/*   Updated: 2020/02/20 14:51:22 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#define HEX_CS_LC "0123456789abcdef"
#define HEX_CS_UC "0123456789ABCDEF"

static int		get_pos_in_array(const char c)
{
	int i;

	i = 0;
	while (i < 16 && HEX_CS_LC[i] != c && HEX_CS_UC[i] != c)
		i++;
	return (i);
}

static size_t	ft_atoi_base_routine(const char *nb)
{
	int		i;
	int		nblen;
	size_t	res;

	nblen = ft_strlen(nb) - 1;
	i = -1;
	res = 0;
	while (nb[++i])
		res += ft_pow(16, nblen - i) * get_pos_in_array(nb[i]);
	return (res);
}

size_t			ft_atoi_hex(const char *nb)
{
	int i;

	i = 0;
	while (ft_iswhitespace(nb[i]))
		i++;
	return (ft_atoi_base_routine(nb + i));
}
