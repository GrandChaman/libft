/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/28 16:56:39 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/28 18:56:58 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memdup(void *s1, size_t len)
{
	uint8_t	*res;

	res = ft_memalloc(len);
	if (!res)
		return (NULL);
	ft_memmove(res, s1, len);
	return (res);
}
