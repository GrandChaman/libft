/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dbuf_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 11:02:25 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 13:35:34 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int		dbuf_init(t_dbuf *buf)
{
	if (!buf)
		return (LIBFT_ERR);
	buf->buf = ft_strnew(BUFF_SIZE);
	buf->len = BUFF_SIZE;
	buf->cursor = 0;
	return (LIBFT_OK);
}

int		pdbuf_init(t_pdbuf *buf, int fd)
{
	if (!buf)
		return (LIBFT_ERR);
	ft_bzero(buf->buf, BUFF_SIZE);
	buf->len = BUFF_SIZE;
	buf->cursor = 0;
	buf->fd = fd;
	buf->total = 0;
	return (LIBFT_OK);
}
