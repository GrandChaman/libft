/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_perror.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/03 14:02:34 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/21 15:48:36 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <unistd.h>

void		ft_perror(char *title, char *error)
{
	ft_fprintf(STDERR_FILENO, "%s: %s: %s\n", PRGRM_NAME, title, error);
	exit(-1);
}

void		ft_perror_fixed(char *message, size_t len)
{
	write(STDERR_FILENO, message, len);
	exit(-1);
}
