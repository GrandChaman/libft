/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dbuf_print.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 16:41:16 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 10:31:31 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

int		dbuf_print(t_dbuf *buf, unsigned char fd)
{
	if (!buf)
		return (LIBFT_ERR);
	if (write(fd, buf->buf, buf->cursor) < 0)
		return (LIBFT_ERR);
	return (LIBFT_OK);
}

int		pdbuf_print(t_pdbuf *buf)
{
	if (!buf)
		return (LIBFT_ERR);
	if (write(buf->fd, buf->buf, buf->cursor) < 0)
		return (LIBFT_ERR);
	buf->total += buf->cursor;
	pdbuf_clear(buf);
	return (LIBFT_OK);
}
