/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64.test.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/09 18:04:54 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

void f(void *f)
{
	cr_assert(f);
}

ssize_t cmp(void *i1, void *i2)
{
	return ((int)i2 - (int)i1);
}

Test(SimpleBinaryTree, CreateARootNode)
{
	t_btree *tree;

	tree = NULL;
	tree = bt_create_node("HelloWorld", NULL);
	cr_assert(tree->parent == NULL);
	cr_assert(tree->left == NULL);
	cr_assert(tree->right == NULL);
	cr_assert_str_eq(tree->value, "HelloWorld");
}

Test(SimpleBinaryTree, CreateAChildNode)
{
	t_btree *tree;

	tree = NULL;
	tree = bt_create_node("HelloWorld", NULL);
	cr_assert(tree->parent == NULL);
	cr_assert(tree->left == NULL);
	cr_assert(tree->right == NULL);
	cr_assert_str_eq(tree->value, "HelloWorld");
	tree->left = bt_create_node("HelloWorld2", tree);
	cr_assert(tree->parent == NULL);
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->left == NULL);
	cr_assert(tree->left->right == NULL);
	cr_assert(tree->left->parent == tree);
	cr_assert_str_eq(tree->left->value, "HelloWorld2");
	cr_assert(tree->right == NULL);
	cr_assert_str_eq(tree->value, "HelloWorld");
}

Test(SimpleBinaryTree, RemoveRootNodeWithoutChild)
{
	t_btree *tree;

	tree = bt_create_node("HelloWorld", NULL);
	bt_remove_node(&tree, &f);
	cr_assert(tree == NULL);
}

Test(SimpleBinaryTree, RemoveLeafNode)
{
	t_btree *tree;

	tree = bt_create_node("HelloWorld", NULL);
	tree->left = bt_create_node("HelloWorld", tree);
	tree->left->right = bt_create_node("HelloWorld", tree->left);
	bt_remove_node(&tree->left->right, &f);
	cr_assert(tree != NULL);
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->right == NULL);
}

Test(SimpleBinaryTree, RemoveNodeWithOneChild)
{
	t_btree *tree;

	tree = bt_create_node((void*)0, NULL);
	tree->left = bt_create_node((void*)1, tree);
	tree->left->right = bt_create_node((void*)2, tree->left);
	bt_remove_node(&tree->left, &f);
	cr_assert(tree != NULL);
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)2);
}

Test(SimpleBinaryTree, RemoveNodeWithTwoChild)
{
	t_btree *tree;

	tree = bt_create_node((void*)0, NULL);
	tree->left = bt_create_node((void*)1, tree);
	tree->left->right = bt_create_node((void*)2, tree->left);
	tree->left->left = bt_create_node((void*)3, tree->left);
	bt_remove_node(&tree->left, &f);
	cr_assert(tree != NULL);
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)2);
	cr_assert(tree->left->right == NULL);
	cr_assert(tree->left->left != NULL);
	cr_assert(tree->left->left->value == (void*)3);
}

Test(SimpleBinaryTree, RemoveRootNodeWithSubtree)
{
	t_btree *tree;

	tree = bt_create_node((void*)0, NULL);
	tree->left = bt_create_node((void*)1, tree);
	tree->right = bt_create_node((void*)2, tree);
	tree->left->right = bt_create_node((void*)3, tree->left);
	tree->left->left = bt_create_node((void*)4, tree->left);
	bt_remove_node(&tree, &f);
	cr_assert(tree != NULL);
	cr_assert(tree->left != NULL);
	cr_assert(tree->right == NULL);
	cr_assert(tree->left->value == (void*)1);
	cr_assert(tree->left->right != NULL);
	cr_assert(tree->left->left != NULL);
	cr_assert(tree->left->left->value == (void*)4);
	cr_assert(tree->left->right->value == (void*)3);
}

Test(SimpleBinaryTree, Insert1)
{
	t_btree *tree;

	tree = NULL;
	bt_insert_node(&tree, (void*)5, &cmp);
	cr_assert(tree != NULL);
	cr_assert(tree->value == (void*)5);
	cr_assert(tree->left == NULL);
	cr_assert(tree->right == NULL);
}

Test(SimpleBinaryTree, Insert2)
{
	t_btree *tree;

	tree = NULL;
	bt_insert_node(&tree, (void*)5, &cmp);
	bt_insert_node(&tree, (void*)4, &cmp);
	cr_assert(tree != NULL);
	cr_assert(tree->value == (void*)5);
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)4);
	cr_assert(tree->right == NULL);
}

Test(SimpleBinaryTree, Insert3)
{
	t_btree *tree;

	tree = NULL;
	bt_insert_node(&tree, (void*)5, &cmp);
	bt_insert_node(&tree, (void*)4, &cmp);
	bt_insert_node(&tree, (void*)3, &cmp);
	cr_assert(tree != NULL);
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->left != NULL);
	cr_assert(tree->left->left->value == (void*)3);
}

Test(SimpleBinaryTree, Insert4)
{
	t_btree *tree;

	tree = NULL;
	bt_insert_node(&tree, (void*)5, &cmp);
	bt_insert_node(&tree, (void*)4, &cmp);
	bt_insert_node(&tree, (void*)3, &cmp);
	bt_insert_node(&tree, (void*)6, &cmp);
	cr_assert(tree != NULL);
	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)6);
}

Test(SimpleBinaryTree, Insert4AndADuplicate)
{
	t_btree *tree;

	tree = NULL;
	bt_insert_node(&tree, (void*)5, &cmp);
	bt_insert_node(&tree, (void*)4, &cmp);
	bt_insert_node(&tree, (void*)3, &cmp);
	bt_insert_node(&tree, (void*)6, &cmp);
	bt_insert_node(&tree, (void*)6, &cmp);
	cr_assert(tree != NULL);
	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)6);
	cr_assert(tree->right->right != NULL);
	cr_assert(tree->right->right->value == (void*)6);
}

Test(SimpleBinaryTree, GoLeftMost)
{
	t_btree *tree;
	size_t	some_array[10] = {23, 65, 42, 12, 10, 3, 1, 2, 99, 109};

	tree = NULL;
	bt_insert_node(&tree, (void*)some_array[0], &cmp);
	bt_insert_node(&tree, (void*)some_array[1], &cmp);
	bt_insert_node(&tree, (void*)some_array[2], &cmp);
	bt_insert_node(&tree, (void*)some_array[3], &cmp);
	bt_insert_node(&tree, (void*)some_array[4], &cmp);
	bt_insert_node(&tree, (void*)some_array[5], &cmp);
	bt_insert_node(&tree, (void*)some_array[6], &cmp);
	bt_insert_node(&tree, (void*)some_array[7], &cmp);
	bt_insert_node(&tree, (void*)some_array[8], &cmp);
	bt_insert_node(&tree, (void*)some_array[9], &cmp);
	btree_leftmost(&tree);
	cr_assert(tree->value == (void*)1);
}

Test(SimpleBinaryTree, ResetToRoot)
{
	t_btree *tree;
	size_t	some_array[10] = {23, 65, 42, 12, 10, 3, 1, 2, 99, 109};

	tree = NULL;
	bt_insert_node(&tree, (void*)some_array[0], &cmp);
	bt_insert_node(&tree, (void*)some_array[1], &cmp);
	bt_insert_node(&tree, (void*)some_array[2], &cmp);
	bt_insert_node(&tree, (void*)some_array[3], &cmp);
	bt_insert_node(&tree, (void*)some_array[4], &cmp);
	bt_insert_node(&tree, (void*)some_array[5], &cmp);
	bt_insert_node(&tree, (void*)some_array[6], &cmp);
	bt_insert_node(&tree, (void*)some_array[7], &cmp);
	bt_insert_node(&tree, (void*)some_array[8], &cmp);
	bt_insert_node(&tree, (void*)some_array[9], &cmp);
	btree_leftmost(&tree);
	btree_reset_to_root(&tree);
	cr_assert(tree->value == (void*)23);
}

Test(SimpleBinaryTree, GetNext)
{
	t_btree *tree;
	size_t	some_array[10] = {1, 2, 3, 10, 12, 23, 42, 65, 99, 109};
	int i = 0;

	tree = NULL;
	bt_insert_node(&tree, (void*)some_array[5], &cmp);
	bt_insert_node(&tree, (void*)some_array[4], &cmp);
	bt_insert_node(&tree, (void*)some_array[3], &cmp);
	bt_insert_node(&tree, (void*)some_array[2], &cmp);
	bt_insert_node(&tree, (void*)some_array[0], &cmp);
	bt_insert_node(&tree, (void*)some_array[1], &cmp);
	bt_insert_node(&tree, (void*)some_array[7], &cmp);
	bt_insert_node(&tree, (void*)some_array[6], &cmp);
	bt_insert_node(&tree, (void*)some_array[8], &cmp);
	bt_insert_node(&tree, (void*)some_array[9], &cmp);
	btree_leftmost(&tree);
	do
	{
		cr_assert(tree->value == (void*)some_array[i++]);
	} while (btree_next(&tree));
}

Test(RedBlackTree, RotateLeft)
{
	t_btree *tree;
	size_t	some_array[7] = {5, 2, 10, 8, 12, 6, 9};

	tree = NULL;
	bt_insert_node(&tree, (void*)some_array[0], &cmp);
	bt_insert_node(&tree, (void*)some_array[1], &cmp);
	bt_insert_node(&tree, (void*)some_array[2], &cmp);
	bt_insert_node(&tree, (void*)some_array[3], &cmp);
	bt_insert_node(&tree, (void*)some_array[4], &cmp);
	bt_insert_node(&tree, (void*)some_array[5], &cmp);
	bt_insert_node(&tree, (void*)some_array[6], &cmp);
	rbt_left_rotate(tree);
	btree_reset_to_root(&tree);
	cr_assert(tree->value == (void*)10);
	cr_assert(tree->left->value == (void*)5);
	cr_assert(tree->right->value == (void*)12);
	cr_assert(tree->left->left->value == (void*)2);
	cr_assert(tree->left->right->value == (void*)8);
	cr_assert(tree->left->right->left->value == (void*)6);
	cr_assert(tree->left->right->right->value == (void*)9);
}

Test(RedBlackTree, RotateLeftWithNoChild)
{
	t_btree *tree;

	tree = NULL;
	bt_insert_node(&tree, (void*)1, &cmp);
	bt_insert_node(&tree, (void*)2, &cmp);
	bt_insert_node(&tree, (void*)3, &cmp);
	rbt_left_rotate(tree);
	btree_reset_to_root(&tree);
	cr_assert(tree->value == (void*)2);
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->parent == tree);
	cr_assert(tree->left->value == (void*)1);
	cr_assert(tree->right != NULL);
	cr_assert(tree->right->parent == tree);
	cr_assert(tree->right->value == (void*)3);
}

Test(RedBlackTree, RotateRight)
{
	t_btree *tree;
	size_t	some_array[7] = {10, 5, 12, 2, 8, 6, 9};

	tree = NULL;
	bt_insert_node(&tree, (void*)some_array[0], &cmp);
	bt_insert_node(&tree, (void*)some_array[1], &cmp);
	bt_insert_node(&tree, (void*)some_array[2], &cmp);
	bt_insert_node(&tree, (void*)some_array[3], &cmp);
	bt_insert_node(&tree, (void*)some_array[4], &cmp);
	bt_insert_node(&tree, (void*)some_array[5], &cmp);
	bt_insert_node(&tree, (void*)some_array[6], &cmp);
	rbt_right_rotate(tree);
	btree_reset_to_root(&tree);
	cr_assert(tree->value == (void*)5);
	cr_assert(tree->left->value == (void*)2);
	cr_assert(tree->right->value == (void*)10);
	cr_assert(tree->right->right->value == (void*)12);
	cr_assert(tree->right->left->value == (void*)8);
	cr_assert(tree->right->left->left->value == (void*)6);
	cr_assert(tree->right->left->right->value == (void*)9);
}

Test(RedBlackTree, RotateRightWithNoChild)
{
	t_btree *tree;

	tree = NULL;
	bt_insert_node(&tree, (void*)3, &cmp);
	bt_insert_node(&tree, (void*)2, &cmp);
	bt_insert_node(&tree, (void*)1, &cmp);
	rbt_right_rotate(tree);
	btree_reset_to_root(&tree);
	cr_assert(tree->value == (void*)2);
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->parent == tree);
	cr_assert(tree->left->value == (void*)1);
	cr_assert(tree->right != NULL);
	cr_assert(tree->right->parent == tree);
	cr_assert(tree->right->value == (void*)3);
}

Test(RedBlackTree, InsertSortedAscending)
{
	t_btree *tree;

	tree = NULL;
	rbt_insert_node(&tree, (void*)1, &cmp);
	rbt_insert_node(&tree, (void*)2, &cmp);
	rbt_insert_node(&tree, (void*)3, &cmp);
	rbt_insert_node(&tree, (void*)4, &cmp);
	rbt_insert_node(&tree, (void*)5, &cmp);
	rbt_insert_node(&tree, (void*)6, &cmp);
	rbt_insert_node(&tree, (void*)7, &cmp);
	rbt_insert_node(&tree, (void*)8, &cmp);
	cr_assert(tree->value == (void*)4);
	
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)2);
	cr_assert(tree->left->color == RBT_RED);

	cr_assert(tree->left->left != NULL);
	cr_assert(tree->left->left->value == (void*)1);
	cr_assert(tree->left->left->color == RBT_BLACK);

	cr_assert(tree->left->right != NULL);
	cr_assert(tree->left->right->value == (void*)3);
	cr_assert(tree->left->right->color == RBT_BLACK);

	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)6);
	cr_assert(tree->right->color == RBT_RED);

	cr_assert(tree->right->left != NULL);
	cr_assert(tree->right->left->value == (void*)5);
	cr_assert(tree->right->left->color == RBT_BLACK);

	cr_assert(tree->right->right != NULL);
	cr_assert(tree->right->right->value == (void*)7);
	cr_assert(tree->right->right->color == RBT_BLACK);

	cr_assert(tree->right->right->right != NULL);
	cr_assert(tree->right->right->right->value == (void*)8);
	cr_assert(tree->right->right->right->color == RBT_RED);
}

Test(RedBlackTree, InsertSortedDesc)
{
	t_btree *tree;

	tree = NULL;
	rbt_insert_node(&tree, (void*)8, &cmp);
	rbt_insert_node(&tree, (void*)7, &cmp);
	rbt_insert_node(&tree, (void*)6, &cmp);
	rbt_insert_node(&tree, (void*)5, &cmp);
	rbt_insert_node(&tree, (void*)4, &cmp);
	rbt_insert_node(&tree, (void*)3, &cmp);
	rbt_insert_node(&tree, (void*)2, &cmp);
	rbt_insert_node(&tree, (void*)1, &cmp);
	cr_assert(tree->value == (void*)5);
	
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)3);
	cr_assert(tree->left->color == RBT_RED);

	cr_assert(tree->left->left != NULL);
	cr_assert(tree->left->left->value == (void*)2);
	cr_assert(tree->left->left->color == RBT_BLACK);

	cr_assert(tree->left->left->left != NULL);
	cr_assert(tree->left->left->left->value == (void*)1);
	cr_assert(tree->left->left->left->color == RBT_RED);

	cr_assert(tree->left->right != NULL);
	cr_assert(tree->left->right->value == (void*)4);
	cr_assert(tree->left->right->color == RBT_BLACK);

	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)7);
	cr_assert(tree->right->color == RBT_RED);

	cr_assert(tree->right->left != NULL);
	cr_assert(tree->right->left->value == (void*)6);
	cr_assert(tree->right->left->color == RBT_BLACK);

	cr_assert(tree->right->right != NULL);
	cr_assert(tree->right->right->value == (void*)8);
	cr_assert(tree->right->right->color == RBT_BLACK);
}

Test(RedBlackTree, InsertRandom)
{
	t_btree *tree;

	tree = NULL;
	rbt_insert_node(&tree, (void*)319, &cmp);
	rbt_insert_node(&tree, (void*)649, &cmp);
	rbt_insert_node(&tree, (void*)976, &cmp);
	rbt_insert_node(&tree, (void*)479, &cmp);
	rbt_insert_node(&tree, (void*)759, &cmp);
	rbt_insert_node(&tree, (void*)631, &cmp);
	rbt_insert_node(&tree, (void*)235, &cmp);
	rbt_insert_node(&tree, (void*)247, &cmp);
	rbt_insert_node(&tree, (void*)735, &cmp);
	rbt_insert_node(&tree, (void*)284, &cmp);
	rbt_insert_node(&tree, (void*)678, &cmp);
	rbt_insert_node(&tree, (void*)769, &cmp);
	rbt_insert_node(&tree, (void*)752, &cmp);
	rbt_insert_node(&tree, (void*)786, &cmp);

	cr_assert(tree->value == (void*)479);
	
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)247);
	cr_assert(tree->left->color == RBT_BLACK);

	cr_assert(tree->left->left != NULL);
	cr_assert(tree->left->left->value == (void*)235);
	cr_assert(tree->left->left->color == RBT_BLACK);

	cr_assert(tree->left->right != NULL);
	cr_assert(tree->left->right->value == (void*)319);
	cr_assert(tree->left->right->color == RBT_BLACK);

	cr_assert(tree->left->right->left != NULL);
	cr_assert(tree->left->right->left->value == (void*)284);
	cr_assert(tree->left->right->left->color == RBT_RED);

	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)649);
	cr_assert(tree->right->color == RBT_BLACK);

	cr_assert(tree->right->left != NULL);
	cr_assert(tree->right->left->value == (void*)631);
	cr_assert(tree->right->left->color == RBT_BLACK);

	cr_assert(tree->right->right != NULL);
	cr_assert(tree->right->right->value == (void*)759);
	cr_assert(tree->right->right->color == RBT_RED);

	cr_assert(tree->right->right->left != NULL);
	cr_assert(tree->right->right->left->value == (void*)735);
	cr_assert(tree->right->right->left->color == RBT_BLACK);

	cr_assert(tree->right->right->left->left != NULL);
	cr_assert(tree->right->right->left->left->value == (void*)678);
	cr_assert(tree->right->right->left->left->color == RBT_RED);

	cr_assert(tree->right->right->left->right != NULL);
	cr_assert(tree->right->right->left->right->value == (void*)752);
	cr_assert(tree->right->right->left->right->color == RBT_RED);

	cr_assert(tree->right->right->right != NULL);
	cr_assert(tree->right->right->right->value == (void*)786);
	cr_assert(tree->right->right->right->color == RBT_BLACK);

	cr_assert(tree->right->right->right->left != NULL);
	cr_assert(tree->right->right->right->left->value == (void*)769);
	cr_assert(tree->right->right->right->left->color == RBT_RED);

	cr_assert(tree->right->right->right->right != NULL);
	cr_assert(tree->right->right->right->right->value == (void*)976);
	cr_assert(tree->right->right->right->right->color == RBT_RED);
}

Test(RedBlackTree, RightEdge)
{
	t_btree *tree;

	tree = NULL;
	rbt_insert_node(&tree, (void*)5, &cmp);
	rbt_insert_node(&tree, (void*)9, &cmp);
	rbt_insert_node(&tree, (void*)7, &cmp);

	cr_assert(tree->value == (void*)7);
	cr_assert(tree->color == RBT_BLACK);
	
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)5);
	cr_assert(tree->left->color == RBT_RED);

	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)9);
	cr_assert(tree->right->color == RBT_RED);
}

Test(RedBlackTree, LeftEdge)
{
	t_btree *tree;

	tree = NULL;
	rbt_insert_node(&tree, (void*)5, &cmp);
	rbt_insert_node(&tree, (void*)2, &cmp);
	rbt_insert_node(&tree, (void*)3, &cmp);

	cr_assert(tree->value == (void*)3);
	cr_assert(tree->color == RBT_BLACK);
	
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)2);
	cr_assert(tree->left->color == RBT_RED);

	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)5);
	cr_assert(tree->right->color == RBT_RED);
}

Test(RedBlackTree, RightLine)
{
	t_btree *tree;

	tree = NULL;
	rbt_insert_node(&tree, (void*)1, &cmp);
	rbt_insert_node(&tree, (void*)2, &cmp);
	rbt_insert_node(&tree, (void*)3, &cmp);

	cr_assert(tree->value == (void*)2);
	cr_assert(tree->color == RBT_BLACK);
	
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)1);
	cr_assert(tree->left->color == RBT_RED);

	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)3);
	cr_assert(tree->right->color == RBT_RED);
}

Test(RedBlackTree, LeftLine)
{
	t_btree *tree;

	tree = NULL;
	rbt_insert_node(&tree, (void*)9, &cmp);
	rbt_insert_node(&tree, (void*)8, &cmp);
	rbt_insert_node(&tree, (void*)7, &cmp);

	cr_assert(tree->value == (void*)8);
	cr_assert(tree->color == RBT_BLACK);
	
	cr_assert(tree->left != NULL);
	cr_assert(tree->left->value == (void*)7);
	cr_assert(tree->left->color == RBT_RED);

	cr_assert(tree->right != NULL);
	cr_assert(tree->right->value == (void*)9);
	cr_assert(tree->right->color == RBT_RED);
}