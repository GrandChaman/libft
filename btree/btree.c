/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 14:43:51 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/10 11:26:21 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_btree	*bt_create_node(void *value, t_btree *parent)
{
	t_btree *res;

	res = ft_memalloc(sizeof(t_btree));
	res->value = value;
	res->parent = parent;
	res->color = RBT_RED;
	return (res);
}

void	*bt_destroy_node(t_btree **node, void f(void *))
{
	if (f)
		f((*node)->value);
	ft_memdel((void**)node);
	*node = NULL;
	return (NULL);
}

void	bt_remove_node(t_btree **node, void f(void *))
{
	t_btree *next;
	t_btree *backup;

	backup = *node;
	if (!(*node)->left && !(*node)->right)
		bt_replace_reference_in_parent(backup, bt_destroy_node(node, f));
	else if (!!(*node)->left ^ !!(*node)->right)
	{
		backup = bt_replace_reference_in_parent(backup,
			(backup->left ? backup->left : backup->right));
		bt_destroy_node(node, f);
		*node = backup;
	}
	else
	{
		next = (*node)->right;
		if (next != NULL)
			while (next->left)
				next = next->left;
		backup->value = next->value;
		bt_remove_node(&next, f);
	}
}

t_btree	*bt_insert_node(t_btree **tree, void *v, ssize_t (f)(void *, void *))
{
	ssize_t diff;

	if (!tree)
		return (NULL);
	if (!*tree)
		return (*tree = bt_create_node(v, NULL));
	diff = f((*tree)->value, v);
	if (diff < 0)
		return ((*tree)->left ? bt_insert_node(&(*tree)->left, v, f) :
			((*tree)->left = bt_create_node(v, (*tree))));
	else
		return ((*tree)->right ? bt_insert_node(&(*tree)->right, v, f) :
			((*tree)->right = bt_create_node(v, (*tree))));
}

void	bt_destroy(t_btree **tree, void (*f)(void *))
{
	if (!tree || !*tree)
		return ;
	if ((*tree)->left)
		bt_destroy(&((*tree)->left), f);
	if (f)
		f((*tree)->value);
	if ((*tree)->right)
		bt_destroy(&((*tree)->right), f);
	ft_memdel((void**)tree);
}
