/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_movements.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 14:43:51 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:44:53 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_btree		*btree_next(t_btree **tree)
{
	t_btree *cursor;
	t_btree *parent_cursor;

	if (!tree || !*tree)
		return (NULL);
	cursor = *tree;
	if (cursor->right && (cursor = cursor->right))
	{
		while (cursor->left)
			cursor = cursor->left;
		return (*tree = cursor);
	}
	parent_cursor = cursor->parent;
	if (!parent_cursor)
		return (NULL);
	if (parent_cursor->left == cursor)
		return (*tree = parent_cursor);
	while (parent_cursor && parent_cursor->right == cursor)
	{
		if (!cursor->parent)
			return (*tree = cursor);
		cursor = cursor->parent;
		parent_cursor = cursor->parent;
	}
	return (*tree = parent_cursor);
}

t_btree		*btree_reset_to_root(t_btree **tree)
{
	while ((*tree)->parent)
		*tree = (*tree)->parent;
	return (*tree);
}

t_btree		*btree_leftmost(t_btree **tree)
{
	while ((*tree)->left)
		*tree = (*tree)->left;
	return (*tree);
}
