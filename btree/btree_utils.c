/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 17:46:29 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:56:46 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_btree	*bt_replace_reference_in_parent(t_btree *node, t_btree *ref)
{
	if (!node->parent)
		return (NULL);
	if (node->parent->left == node)
		return (node->parent->left = ref);
	else
		return (node->parent->right = ref);
}

void	rbt_insert_node_uncle_and_red(t_btree *uncle, t_btree *cursor)
{
	uncle->color = RBT_BLACK;
	cursor->parent->color = RBT_BLACK;
	cursor->parent->parent->color = RBT_RED;
	cursor = cursor->parent;
}

void	rbt_insert_node_left_parent_line(t_btree *cursor)
{
	t_btree *grand_parent;

	grand_parent = rbt_right_rotate(cursor->parent->parent);
	cursor->parent->color = RBT_BLACK;
	grand_parent->color = RBT_RED;
}

void	rbt_insert_node_right_parent_line(t_btree *cursor)
{
	t_btree *grand_parent;

	grand_parent = rbt_left_rotate(cursor->parent->parent);
	cursor->parent->color = RBT_BLACK;
	grand_parent->color = RBT_RED;
}

void	rbt_apply_rule(t_btree *cursor)
{
	t_btree *uncle;

	if ((uncle = rbt_get_uncle(cursor)) && uncle->color == RBT_RED)
		rbt_insert_node_uncle_and_red(uncle, cursor);
	else if (cursor->parent->left == cursor
		&& cursor->parent->parent->right == cursor->parent)
		cursor = rbt_right_rotate(cursor->parent);
	else if (cursor->parent->right == cursor
		&& cursor->parent->parent->left == cursor->parent)
		cursor = rbt_left_rotate(cursor->parent);
	else if (cursor->parent->left == cursor
		&& cursor->parent->parent->left == cursor->parent)
		rbt_insert_node_left_parent_line(cursor);
	else if (cursor->parent->right == cursor
		&& cursor->parent->parent->right == cursor->parent)
		rbt_insert_node_right_parent_line(cursor);
	else
		cursor = cursor->parent;
}
