/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rbtree.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 14:43:51 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:56:14 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_btree	*rbt_left_rotate(t_btree *tree)
{
	void *tmp;

	if (!tree || !tree->right)
		return (NULL);
	tmp = tree->parent;
	tree->right->parent = tmp;
	bt_replace_reference_in_parent(tree, tree->right);
	tree->parent = tree->right;
	tree->right = tree->parent->left;
	if (tree->right)
		tree->right->parent = tree;
	tree->parent->left = tree;
	return (tree);
}

t_btree	*rbt_right_rotate(t_btree *tree)
{
	void *tmp;

	if (!tree || !tree->left)
		return (NULL);
	tmp = tree->parent;
	tree->left->parent = tmp;
	bt_replace_reference_in_parent(tree, tree->left);
	tree->parent = tree->left;
	tree->left = tree->parent->right;
	if (tree->left)
		tree->left->parent = tree;
	tree->parent->right = tree;
	return (tree);
}

t_btree	*rbt_get_uncle(t_btree *tree)
{
	if (!tree || !tree->parent || !tree->parent->parent)
		return (NULL);
	if (tree->parent->parent->left == tree->parent)
		return (tree->parent->parent->right);
	return (tree->parent->parent->left);
}

t_btree	*rbt_insert_node(t_btree **tree, void *v, ssize_t (f)(void *, void *))
{
	t_btree	*cursor;

	cursor = bt_insert_node(tree, v, f);
	while (cursor->parent && cursor->parent->parent
		&& cursor->parent->color == RBT_RED)
	{
		if (cursor->color != RBT_RED)
		{
			cursor = cursor->parent;
			continue ;
		}
		rbt_apply_rule(cursor);
		btree_reset_to_root(tree);
		(*tree)->color = RBT_BLACK;
	}
	return (cursor);
}
