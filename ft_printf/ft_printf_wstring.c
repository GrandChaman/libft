/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_wstring.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 11:04:53 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/27 16:54:35 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_utils.h"
#include <stdarg.h>
#include "ft_printf_format_list.h"
#include "libft.h"
#include <wchar.h>
#include <unistd.h>

uint8_t	ft_putwchar(t_ptf_buf *buf, unsigned int c)
{
	int				i;
	unsigned char	byte;
	uint8_t			res;

	byte = c;
	res = 0;
	if (c <= 127 || MB_CUR_MAX == 1)
		res = printf_dbuf_append_char(buf, byte);
	else if (!res && (c < 2048 || MB_CUR_MAX == 2))
		byte = ((((unsigned int)c >> 6) << 27) >> 27 | WCHAR_1);
	else if (!res && ((c > 2047 && c < 65536) || MB_CUR_MAX == 3))
		byte = (((((unsigned int)c >> 12) << 28) >> 28) | WCHAR_2);
	else if (!res)
		byte = (((((unsigned int)c >> 18) << 29) >> 29) | WCHAR_3);
	if (c <= 127 || MB_CUR_MAX == 1)
		return (LIBFT_OK);
	res = printf_dbuf_append_char(buf, byte);
	i = 1 + ((c > 2047 && c < 65536) ? 1 : 0) + (c > 65536 ? 2 : 0);
	i += ((size_t)i >= (size_t)MB_CUR_MAX ? (i - MB_CUR_MAX - 1) : 0);
	while (!res && --i >= 0)
		res = printf_dbuf_append_char(buf,
			((((c >> (i * 6)) << 26) >> 26) | WCHAR_GEN));
	return (res);
}

uint8_t	print_wchar(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param param)
{
	unsigned int		c;
	uint8_t				res;

	res = 0;
	c = va_arg(buf->ap, unsigned int);
	if (!is_utf8(c))
		return (LIBFT_OK);
	if (format.len)
		res = ft_putnstr(buf, format.str, format.len);
	if (c > 255 && c < 2048)
		param.padding -= 1;
	else if ((c > 2047 && c < 65536))
		param.padding -= 2;
	else if (c > 65535 && c < 2097152)
		param.padding -= 3;
	else if (c > 2097151)
		param.padding -= 4;
	if (!res)
		res = handle_padding(buf, param, 1, BEFORE);
	if (!res)
		res = ft_putwchar(buf, c);
	if (!res)
		res = handle_padding(buf, param, 1, AFTER);
	return (res);
}

uint8_t	ft_putwstr(t_ptf_buf *buf, wchar_t *c, int len)
{
	int		i;
	int		o;
	int		tmp;

	i = 0;
	o = 0;
	while (c[i] && (o <= len || len < 0))
	{
		tmp = wchar_length((unsigned int)c[i]);
		if ((o + tmp <= len) || (len < 0))
			if (ft_putwchar(buf, (unsigned int)c[i]) == LIBFT_ERR)
				return (LIBFT_ERR);
		o += tmp;
		i++;
	}
	return (LIBFT_OK);
}

uint8_t	print_wstring(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param p)
{
	wchar_t		*c;
	int			i;
	int			len;
	uint8_t		res;

	len = 0;
	i = -1;
	res = 0;
	if (format.len)
		res = ft_putnstr(buf, format.str, format.len);
	if (!(c = va_arg(buf->ap, wchar_t *)))
		c = L"(null)";
	while (c[++i] && (i < p.precision || p.precision < 0))
		if (!is_utf8((unsigned int)c[i]))
			return (LIBFT_OK);
	len = ft_strwlen(c, p.precision);
	p.padding += (len < p.precision || (len < p.precision && !len)
		? p.precision - len : 0) + (!p.precision ? 1 : 0);
	len = (!len ? 1 : len);
	res = handle_padding(buf, p, len, BEFORE);
	if (!res && !len && p.padding > 0)
		res = print_padding(buf, ' ', 1);
	else if (!res)
		res = ft_putwstr(buf, c, p.precision);
	return (!res ? handle_padding(buf, p, len, AFTER) : res);
}
