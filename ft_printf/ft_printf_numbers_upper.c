/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_numbers_upper.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 15:18:55 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 14:18:40 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_utils.h"
#include <stdarg.h>
#include "ft_printf_format_list.h"
#include "libft.h"

uint8_t	print_octal_upper(t_ptf_buf *buf, t_ptf_toprint format,
	t_ptf_param param)
{
	param.lm = L;
	return (print_octal(buf, format, param));
}

uint8_t	print_unsigned_upper(t_ptf_buf *buf, t_ptf_toprint format,
	t_ptf_param param)
{
	param.lm = L;
	return (print_unsigned(buf, format, param));
}

uint8_t	print_signed_upper(t_ptf_buf *buf, t_ptf_toprint format,
	t_ptf_param param)
{
	param.lm = L;
	return (print_signed(buf, format, param));
}

uint8_t	hex_handler(int mode, t_ptf_buf *buf, t_ptf_toprint format,
	t_ptf_param p)
{
	unsigned long long	n;
	int					len;
	char				*istr;
	uint8_t				res;

	res = 0;
	if (format.len)
		res = ft_putnstr(buf, format.str, format.len);
	n = extract_nb(p, buf->ap);
	istr = ft_itoa_base(n, 16);
	if (mode == LR || mode == PTR)
		ft_strtolower(istr);
	len = ft_strlen(istr);
	p.padding -= (mode == PTR || (p.hashtag && (n || mode == PTR)) ? 2 : 0);
	if (!res && (mode == PTR || (p.hashtag && n)) && p.zero)
		res = ft_putnstr(buf, (mode == LR || mode == PTR ? "0x" : "0X"), 2);
	res = handle_padding(buf, p, (n || p.precision ? len : -1), BEFORE);
	if (!res && (mode == PTR || (p.hashtag && n)) && !p.zero)
		res = ft_putnstr(buf, (mode == LR || mode == PTR ? "0x" : "0X"), 2);
	res = (p.precision > 0 ? print_padding(buf, '0', p.precision - len) : 0);
	res = (!res && (n || p.precision) ? ft_putnstr(buf, istr, len) : res);
	if (!res)
		handle_padding(buf, p, (n || p.precision ? len : -1), AFTER);
	free(istr);
	return (res);
}

uint8_t	print_hex_upper(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param param)
{
	return (hex_handler(UP, buf, format, param));
}
