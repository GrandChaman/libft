/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_dbuf.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 09:38:11 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:57:32 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "ft_printf_utils.h"
#include "libft.h"
#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>

int		printf_dbuf_init(t_ptf_buf *buf, int fd)
{
	if (buf->only_print)
		return (pdbuf_init(&buf->pdbuf, fd));
	return (dbuf_init(&buf->dbuf));
}

int		printf_dbuf_append(t_ptf_buf *buf, char *str)
{
	if (buf->only_print)
		return (pdbuf_append(&buf->pdbuf, str));
	return (dbuf_append(&buf->dbuf, str));
}

int		printf_dbuf_append_char(t_ptf_buf *buf, char c)
{
	if (buf->only_print)
		return (pdbuf_push_char(&buf->pdbuf, c));
	return (dbuf_insert(&buf->dbuf, buf->dbuf.cursor, c));
}

int		printf_dbuf_clear(t_ptf_buf *buf)
{
	if (buf->only_print)
		return (pdbuf_clear(&buf->pdbuf));
	return (dbuf_clear(&buf->dbuf));
}

int		printf_dbuf_print(t_ptf_buf *buf, int fd)
{
	if (buf->only_print)
		return (pdbuf_print(&buf->pdbuf));
	return (dbuf_print(&buf->dbuf, fd));
}
