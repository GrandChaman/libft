/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 09:38:11 by fle-roy           #+#    #+#             */
/*   Updated: 2019/11/06 15:48:57 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "ft_printf_utils.h"
#include "libft.h"
#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>

int		ft_printf(const char *format, ...)
{
	int			res;
	t_ptf_buf	buf;

	if (!format)
		return (-1);
	va_start(buf.ap, format);
	buf.only_print = 1;
	printf_dbuf_init(&buf, STDOUT_FILENO);
	res = ft_printf_routine(&buf, format)
		|| printf_dbuf_print(&buf, STDOUT_FILENO);
	if (res == LIBFT_OK)
		res = buf.pdbuf.total;
	printf_dbuf_clear(&buf);
	va_end(buf.ap);
	return (res);
}

int		ft_fprintf(int fd, const char *format, ...)
{
	int			res;
	t_ptf_buf	buf;

	if (!format || fd < 0)
		return (-1);
	va_start(buf.ap, format);
	buf.only_print = 1;
	printf_dbuf_init(&buf, fd);
	res = ft_printf_routine(&buf, format) || printf_dbuf_print(&buf, fd);
	if (res == LIBFT_OK)
		res = buf.pdbuf.total;
	printf_dbuf_clear(&buf);
	va_end(buf.ap);
	return (res);
}

int		ft_asprintf(char **ret, const char *format, ...)
{
	int			res;
	t_ptf_buf	buf;

	if (!format || !ret)
		return (-1);
	buf.only_print = 0;
	va_start(buf.ap, format);
	dbuf_init(&buf.dbuf);
	res = ft_printf_routine(&buf, format);
	if (res == LIBFT_OK)
		res = buf.dbuf.cursor;
	*ret = buf.dbuf.buf;
	va_end(buf.ap);
	return (res);
}

int		ft_snprintf(char *ret, size_t size, const char *format, ...)
{
	int			res;
	t_ptf_buf	buf;
	size_t		stop;

	if (!format || !ret)
		return (-1);
	va_start(buf.ap, format);
	buf.only_print = 0;
	dbuf_init(&buf.dbuf);
	res = ft_printf_routine(&buf, format);
	stop = (size < buf.dbuf.cursor ? size : buf.dbuf.cursor);
	if (res == LIBFT_OK)
	{
		ft_memcpy(ret, buf.dbuf.buf, stop);
		ret[stop] = 0;
	}
	dbuf_destroy(&buf.dbuf);
	va_end(buf.ap);
	return (res);
}
