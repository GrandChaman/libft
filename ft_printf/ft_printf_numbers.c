/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_numbers.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 15:18:55 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:58:02 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_utils.h"
#include <stdarg.h>
#include "ft_printf_format_list.h"
#include "libft.h"

uint8_t	print_signed(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param p)
{
	unsigned long long	n;
	int					len;
	uint8_t				res;

	res = 0;
	if (format.len && ft_putnstr(buf, format.str, format.len) == LIBFT_ERR)
		return (LIBFT_ERR);
	n = extract_nb_signed(&p, buf->ap);
	len = ft_nblen(n) + (!n ? 1 : 0);
	p.padding += (!p.precision && !n ? 1 : 0);
	p.padding -= (((p.plus || (p.space && (len < p.padding
					|| p.padding <= 0))) && !p.neg) ? 1 : 0);
	if (p.space)
		res = print_padding(buf, (p.neg ? '-' : ' '), 1);
	if (!res && (p.plus || p.neg) && p.zero && !p.space)
		res = print_padding(buf, (p.neg ? '-' : '+'), 1);
	handle_padding(buf, p, len, BEFORE);
	if (!res && (p.plus || p.neg) && !p.zero && !p.space)
		res = print_padding(buf, (p.neg ? '-' : '+'), 1);
	if (!res && p.precision > 0)
		res = print_padding(buf, '0', p.precision - len);
	if (!res && !(!p.precision && !n))
		res = ft_putll(buf, n);
	res = (!res ? handle_padding(buf, p, len, AFTER) : res);
	return (res);
}

uint8_t	print_unsigned(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param param)
{
	unsigned long long	n;
	int					len;
	uint8_t				res;

	res = 0;
	if (format.len)
		res = ft_putnstr(buf, format.str, format.len);
	n = extract_nb(param, buf->ap);
	len = ft_nblen(n);
	len = (n ? len : 1);
	if (!res)
		res = handle_padding(buf, param, len, BEFORE);
	if (!res && param.hashtag)
		res = print_padding(buf, '0', 1);
	if (!res && param.precision > 0)
		res = print_padding(buf, '0', param.precision - len);
	if (!res && (param.precision || n))
		res = ft_putll(buf, n);
	if (!res)
		res = handle_padding(buf, param, len, AFTER);
	return (res);
}

uint8_t	print_octal(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param param)
{
	unsigned long long	n;
	int					len;
	char				*istr;
	uint8_t				res;

	res = 0;
	if (format.len)
		res = ft_putnstr(buf, format.str, format.len);
	n = extract_nb(param, buf->ap);
	istr = ft_itoa_base(n, 8);
	len = ft_strlen(istr);
	param.padding += (!param.precision && !n ? 1 : 0);
	param.padding -= (param.hashtag && n ? 1 : 0);
	if (!res)
		res = handle_padding(buf, param, len, BEFORE);
	if (!res &&
		(param.hashtag && ((n && param.precision <= len) || !param.precision)))
		res = print_padding(buf, '0', 1);
	if (!res && param.precision > 0)
		res = print_padding(buf, '0', param.precision - len);
	if (!res && (n || param.precision))
		res = ft_putnstr(buf, istr, len);
	res = (!res ? handle_padding(buf, param, len, AFTER) : res);
	free(istr);
	return (res);
}

uint8_t	print_hex(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param param)
{
	return (hex_handler(LR, buf, format, param));
}

uint8_t	print_pointer(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param param)
{
	param.hashtag = 1;
	param.lm = L;
	return (hex_handler(PTR, buf, format, param));
}
