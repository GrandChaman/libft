/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_string.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 11:04:53 by fle-roy           #+#    #+#             */
/*   Updated: 2019/11/05 11:24:56 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_utils.h"
#include <stdarg.h>
#include "ft_printf_format_list.h"
#include "libft.h"

char	*get_null_str(int empty)
{
	if (empty)
		return ("");
	return ("(null)");
}

uint8_t	print_string(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param p)
{
	char	*str;
	int		len;
	uint8_t	res;

	res = 0;
	if (p.lm == L)
		return (print_wstring(buf, format, p));
	if (format.len && ft_putnstr(buf, format.str, format.len) == LIBFT_ERR)
		return (LIBFT_ERR);
	if (!(str = va_arg(buf->ap, char *)))
		str = get_null_str(!p.precision);
	len = ft_strnlen(str, p.precision);
	p.padding += (!p.precision && len ? len : 0);
	p.precision = (p.precision > len ? len : p.precision);
	if (!p.minus && p.padding > 0)
		res = print_padding(buf, (p.zero ? '0' : ' '), p.padding
		- (p.precision <= 0 ? len : p.precision));
	if (!res && p.precision && len)
		res = ft_putnstr(buf, str, p.precision < 0 ? 0 : p.precision);
	else if (!res && !p.precision && !ft_strcmp(str, "(null)") && !p.zero)
		res = ft_putnstr(buf, " ", 0);
	if (!res && p.minus && p.padding > 0)
		res = print_padding(buf, (p.zero ? '0' : ' '), p.padding
		- (p.precision <= 0 ? len : p.precision));
	return (res);
}

uint8_t	print_char(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param param)
{
	char	c;
	uint8_t	res;

	res = 0;
	if (param.lm == L)
		return (print_wchar(buf, format, param));
	if (format.len && ft_putnstr(buf, format.str, format.len) == LIBFT_ERR)
		return (LIBFT_ERR);
	c = (char)va_arg(buf->ap, int);
	if (!param.minus && param.padding > 0)
		res = print_padding(buf, (param.zero ? '0' : ' '), param.padding - 1);
	if (!res && c)
		res = ft_putnstr(buf, &c, 1);
	else if (!res)
		res = printf_dbuf_append_char(buf, '\0');
	if (!res && param.minus && param.padding > 0)
		res = print_padding(buf, (param.zero ? '0' : ' '), param.padding
			- (param.precision > 0 ? param.precision : 1));
	return (res);
}

uint8_t	print_pourcent(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param param)
{
	uint8_t	res;

	res = 0;
	if (format.len && ft_putnstr(buf, format.str, format.len) == LIBFT_ERR)
		return (LIBFT_ERR);
	if (!param.minus && param.padding > 0)
		res = print_padding(buf, (param.zero ? '0' : ' '), param.padding - 1);
	if (!res)
		res = ft_putnstr(buf, "%", 0);
	if (!res && param.minus && param.padding > 0)
		res = print_padding(buf, ' ', param.padding - 1);
	return (res);
}
