/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_numbers2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 15:18:55 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 14:56:39 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_utils.h"
#include <stdarg.h>
#include "ft_printf_format_list.h"
#include "libft.h"

uint8_t	print_binary(t_ptf_buf *buf, t_ptf_toprint format, t_ptf_param p)
{
	unsigned long long	n;
	int					len;
	char				*istr;
	uint8_t				res;

	res = 0;
	if (format.len)
		res = ft_putnstr(buf, format.str, format.len);
	n = extract_nb(p, buf->ap);
	istr = ft_itoa_base(n, 2);
	len = ft_strlen(istr);
	p.padding -= ((p.hashtag && n) ? 2 : 0);
	if (!res && (p.hashtag && n) && p.zero)
		res = ft_putnstr(buf, "0b", 2);
	if (!res)
		res = handle_padding(buf, p, (n || p.precision ? len : -1), BEFORE);
	if (!res && (p.hashtag && n) && !p.zero)
		res = ft_putnstr(buf, "0b", 2);
	(p.precision > 0 ? print_padding(buf, '0', p.precision - len) : 0);
	if (!res && (n || p.precision))
		res = ft_putnstr(buf, istr, len);
	if (!res)
		res = handle_padding(buf, p, (n || p.precision ? len : -1), AFTER);
	free(istr);
	return (res);
}
