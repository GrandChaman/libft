/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 09:53:42 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 13:54:47 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_utils.h"
#include <unistd.h>
#include "libft.h"
#include <stdlib.h>

int					ft_nblen(unsigned long long nb)
{
	if (!nb)
		return (0);
	return (ft_nblen(nb / 10) + 1);
}

uint8_t				print_padding(t_ptf_buf *buf, char c, int len)
{
	if (len <= 0)
		return (LIBFT_OK);
	while (len--)
		if (printf_dbuf_append_char(buf, c))
			return (LIBFT_ERR);
	return (LIBFT_OK);
}

int					ft_strccmp(const char *lhs, const char *rhs)
{
	int i;

	i = 0;
	while (lhs[i] == rhs[i] && lhs[i] && rhs[i])
		i++;
	return (i);
}

int					ft_strnccmp(const char *lhs, const char *rhs, int max)
{
	int i;

	i = 0;
	while (lhs[i] == rhs[i] && lhs[i] && rhs[i] && i < max)
		i++;
	return (i);
}

uint8_t				ft_putnstr(t_ptf_buf *buf, const char *str, int stop)
{
	if (!stop)
		stop = ft_strlen(str);
	return (printf_dbuf_append_w_len(buf, (char*)str, stop));
}
