/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pdbuf.test.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 10:36:48 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 11:30:24 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <criterion/criterion.h>
#include <criterion/redirect.h>

void redirect_all_stdoutput(void)
{
        cr_redirect_stdout();
        cr_redirect_stderr();
}

Test(PDBuf, SimpleAppend, .init=redirect_all_stdoutput)
{
    t_pdbuf buf;

	pdbuf_init(&buf, 1);
	pdbuf_append(&buf, "HelloWorld");
	pdbuf_print(&buf);
	cr_assert_stdout_eq_str("HelloWorld");
}

Test(PDBuf, MultipleAppend, .init=redirect_all_stdoutput)
{
    t_pdbuf buf;

	pdbuf_init(&buf, 1);
	pdbuf_append(&buf, "HelloWorld");
	pdbuf_append(&buf, "SalutSalut");
	pdbuf_print(&buf);
	cr_assert_stdout_eq_str("HelloWorldSalutSalut");
}

Test(PDBuf, OnAnotherFd, .init=redirect_all_stdoutput)
{
    t_pdbuf buf;

	pdbuf_init(&buf, 2);
	pdbuf_append(&buf, "HelloWorld");
	pdbuf_append(&buf, "SalutSalut");
	pdbuf_print(&buf);
	cr_assert_stderr_eq_str("HelloWorldSalutSalut");
}

Test(PDBuf, GoOverBuffsize, .init=redirect_all_stdoutput)
{
    t_pdbuf buf;
    char	string[(BUFF_SIZE * 2) + 1];

	memset(string, 0, (BUFF_SIZE * 2) + 1);
	memset(string, 'a', BUFF_SIZE);
	memset(string + BUFF_SIZE, 'b', BUFF_SIZE - 1);
	pdbuf_init(&buf, 1);
	pdbuf_append(&buf, string);
	pdbuf_print(&buf);
	cr_assert_stdout_eq_str(string);
}

Test(PDBuf, PushChar, .init=redirect_all_stdoutput)
{
    t_pdbuf buf;
    char	string[4] = "abc";

	pdbuf_init(&buf, 1);
	pdbuf_push_char(&buf, string[0]);
	pdbuf_push_char(&buf, string[1]);
	pdbuf_push_char(&buf, string[2]);
	pdbuf_print(&buf);
	cr_assert_stdout_eq_str(string);
}